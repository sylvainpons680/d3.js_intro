// Selectionner un élément html
// d3.select("div");
// Selectionner par class
// d3.select(".myclass");
// Lire ce qu'il y as à l'intérieur
// alert(d3.select(".myclass").text());
// Selectionner l'id
// d3.select("#hello");
// Lire
// alert(d3.select("#hello").text());

// Ajouter un span
// d3.select("div.myclass").append("span");
// Ajouter du texte avec le span
d3.select("div.myclass").append("span").text("D3js");
//Ajouter un attribut à l'élement
// d3.select(".myclass").attr("style","color:blue");
d3.select(".myclass").style("color","red");
d3.select(".myclass").classed("myclass",false);

